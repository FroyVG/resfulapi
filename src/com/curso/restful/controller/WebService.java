package com.curso.restful.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.curso.restful.model.Usuario;
import com.curso.restful.model.dao.UsuarioDao;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Clase Controller que gestiona las peticiones de servicios REST.
 * 
 * @since 2019-06-23
 * @author Froy
 * @version 1.0
 */
@Path("/")
public class WebService {

	private static List<Usuario> usuarios = new ArrayList<Usuario>();

	@GET
	@Path("/dataText")
	@Produces("text/plain")
	public String dataText() {
		return "conectado a mi servidor type text";
	}

	@GET
	@Path("/dataJson")
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario dataJson() {
		return new Usuario(1, "Froylan", "Villaverde", "froy.villaverde@gmail.com", 20);
	}

	@GET
	@Path("/leerTodosLosUsuarios")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Usuario> leerTodosLosUsuarios() {
		UsuarioDao dao = new UsuarioDao();
		return  dao.selecAll();
	}

	@POST
	@Path("/insertaUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON) 
	public Response insertaUsuario(Usuario usuario) {
		UsuarioDao dao = new UsuarioDao();
		Usuario usuario2 = dao.guardaUsuario(usuario);
		return Response.status(200).entity(usuario2).build();
	}
	
	@POST
	@Path("/editaUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editaUsuario(Usuario usuario) {
		UsuarioDao dao = new UsuarioDao();
		dao.editaUsuario(usuario);
		return Response.status(200).entity("ok").build();
	}
	
	@POST
	@Path("/eliminaUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response eliminaUsuario(String id) {
		UsuarioDao dao = new UsuarioDao();
		Usuario usuario;
		usuario = dao.deleteUsuario(Integer.parseInt(id));
		return Response.status(200).entity(usuario).build();
	}

	public static void main(String[] args) {
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://localhost:8083/RestfulApi/rest/insertaUsuario");
			Usuario temporal = new Usuario(8, "Robert", "Palazueleishon", "rob.pala@gmail.com", 30);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, temporal);//JSON: "{\'id\':\'278\'}"
			String output = response.getEntity(String.class);
			System.out.println(output);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
