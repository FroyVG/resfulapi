package com.curso.restful.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
/**
 * Clase utiler�a que construye la conexi�n a base de datos.
 * 
 * @author Froy
 * @version 1.0
 * @since 2019-06-23
 */
public class IbatisUtil {
	private static final SqlSessionFactory SQL_SESSION_FACTORY = construirSessionFactory();

	private static SqlSessionFactory construirSessionFactory() {

		SqlSessionFactory sessionFactory = null;
		String resource = "SqlMapConfig.xml";

		try {
			Reader reader = Resources.getResourceAsReader(resource);
			if (sessionFactory == null) {
				sessionFactory = new SqlSessionFactoryBuilder().build(reader);
				sessionFactory.getConfiguration().getTypeAliasRegistry().registerAlias(Usuario.class);
				sessionFactory.getConfiguration().addMapper(UsuarioMapper.class);
			}
		} catch (FileNotFoundException fileNotFoundException) {
			System.out.println("1");
			fileNotFoundException.printStackTrace();
		} catch (IOException iOException) {
			System.out.println("2");
			iOException.printStackTrace();
		} catch (Exception exception) {
			System.out.println("3");
			exception.printStackTrace();
		}
		return sessionFactory;
	}

	public String toString() {
		String temp = "";
		String aux = "                                      ";
		try {
			java.lang.reflect.Field variables[] = this.getClass().getDeclaredFields();
			temp = "\n____________________________________________________________________________________________________\n"
					+ "Propiedades del objeto " + this.getClass().getName() + " :\n\n" + ">>> Tipo <<<"
					+ aux.substring(">>> Tipo <<<".length()) + ">>> Nombre de Variable <<<"
					+ aux.substring(">>> Nombre de Variable <<<".length()) + ">>> Valor <<<\n";

			for (int i = 0; i < variables.length; i++) {
				temp += variables[i].getType() + aux.substring(variables[i].getType().toString().length())
						+ variables[i].getName() + aux.substring(variables[i].getName().toString().length())
						+ variables[i].get(this) + "\n";
			}

			temp += "____________________________________________________________________________________________________\n";
		} catch (Exception e) {
			return "EXCEPTION:::::::::::::::::::: " + this.getClass().getName()
					+ ".toString() No esta configurado el objeto: \n" + super.toString() + "\n";
		}
		return temp;
	}

	public void destroy() {
		this.finalize();
	}

	protected void finalize() {
		try {
			super.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static SqlSessionFactory getSqlSessionFactory() {
		return SQL_SESSION_FACTORY;
	}

}
