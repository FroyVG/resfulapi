package com.curso.restful.model.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.curso.restful.model.IbatisUtil;
import com.curso.restful.model.Usuario;
import com.curso.restful.model.UsuarioMapper;

public class UsuarioDao {
	private SqlSessionFactory sqlSessionFactory;

	public UsuarioDao() {
		super();
		sqlSessionFactory = IbatisUtil.getSqlSessionFactory();
	}

	public List<Usuario> selecAll() {
		SqlSession session = sqlSessionFactory.openSession();
		try {

			UsuarioMapper mapper = session.getMapper(UsuarioMapper.class);
			List<Usuario> list = mapper.selectAll();
			session.commit();
			return list;
		} finally {
			session.close();
		}

	}

	public Usuario SelectUsuarioById(int id) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			UsuarioMapper mapper = session.getMapper(UsuarioMapper.class);
			Usuario usuario = mapper.SelectUsuarioById(id);
			session.commit();
			return usuario;
		} finally {
			session.close();
		}
	}

	public Usuario guardaUsuario(Usuario usuario) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			UsuarioMapper mapper = session.getMapper(UsuarioMapper.class);
			mapper.guardaUsuario(usuario);
			session.commit();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return usuario;
	}

	public void editaUsuario(Usuario usuario) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			UsuarioMapper mapper = session.getMapper(UsuarioMapper.class);
			mapper.editaUsuario(usuario);
			session.commit();
		} finally {
			session.close();
		}
	}

	public Usuario deleteUsuario(int id) {
		SqlSession session = sqlSessionFactory.openSession();
		Usuario usuario;
		try {
			UsuarioMapper mapper = session.getMapper(UsuarioMapper.class);
			usuario = mapper.SelectUsuarioById(id);
			mapper.deleteUsuario(id);
			session.commit();
		} finally {
			session.close();
		}
		return usuario;
	}

}
