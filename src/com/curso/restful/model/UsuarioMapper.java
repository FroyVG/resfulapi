package com.curso.restful.model;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.SelectKey;

public interface UsuarioMapper {
	
	final String LEER_TODOS_USUARIOS = "SELECT * FROM USUARIO";
	final String LEER_USUARIO_ID="SELECT * FROM USUARIO WHERE ID= #{id}";
	final String ELIMINA_USUARIO_ID = "DELETE FROM USUARIO WHERE ID = #{id}";
	final String GUARDA_USUARIO = "INSERT INTO USUARIO (NOMBRE, APELLIDO, EDAD, CORREO) VALUES (#{nombre}, #{apellido}, #{edad}, #{correo})";
	final String EDITA_USUARIO = "UPDATE USUARIO SET NOMBRE=#{nombre} , APELLIDO=#{apellido} , EDAD=#{edad} , CORREO=#{correo} WHERE ID = #{id}";
	
	@Select(LEER_TODOS_USUARIOS)
	@Results(value= {
			@Result(property="id", column="id"),
			@Result(property="nombre", column="nombre"),
			@Result(property="apellido", column="apellido"),
			@Result(property="edad", column="edad"),
			@Result(property="correo", column="correo")
	})
	List<Usuario> selectAll();
	
	@Select(LEER_USUARIO_ID)
	@Results(value= {
			@Result(property="id", column="id"),
			@Result(property="nombre", column="nombre"),
			@Result(property="apellido", column="apellido"),
			@Result(property="edad", column="edad"),
			@Result(property="correo", column="correo")
	})
	Usuario SelectUsuarioById(int id);
	
	@Delete(ELIMINA_USUARIO_ID)
	void deleteUsuario(int id);
	
	@Insert(GUARDA_USUARIO)
	@SelectKey(statement="select HR.PERSON_SEQ.currval from dual", keyProperty="id", before=false, resultType=int.class)	
	int guardaUsuario(Usuario usuario);
	
	@Update(EDITA_USUARIO)
	int editaUsuario(Usuario usuario);
	
}
